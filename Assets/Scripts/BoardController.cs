﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardController : MonoBehaviour {
    public int columns;
    public int rows;
    private int chanceToGenerate;
    public GameObject[] Dungeon1Floors;
    public GameObject[] Dungeon2Floors;
    public GameObject[] Dungeon1OuterWalls;
    public GameObject[] Dungeon2OuterWalls;
    public GameObject[] Dungeon1WallObstacles;
    public GameObject[] Dungeon2WallObstacles;
    public GameObject[] Dungeon1FoodItems;
    public GameObject[] Dungeon2FoodItems;
    public GameObject[] Dungeon1Enemies;
    public GameObject[] Dungeon2Enemies;
    public GameObject[] PowerUp;
    public GameObject exit;
    private Transform gameBoard;
    private List<Vector3> obstaclesGrid;

	void Awake () {
        obstaclesGrid = new List<Vector3>();
	}
	
	void Update () {
	
	}

    private void InitializeObstaclePositions() {
        obstaclesGrid.Clear();
        for(int x = 2; x < columns- 2; x++)
        {
            for(int y = 2; y < rows - 2; y++)
            {
                obstaclesGrid.Add(new Vector3(x, y, 0f));
            }
        }
    }

    private void SetUpGameBoard()
    {
        gameBoard = new GameObject("Game Board").transform;
        obstaclesGrid.Clear();
        for(int x = 0; x < columns; x++)
        {
            for(int y = 0; y < rows; y++)
            {
                GameObject selectedTile;
                if(x == 0 || y == 0 || x == columns - 1 || y == rows - 1)
                {
                    if(GameController.isDungeon1)
                    {
                        selectedTile = Dungeon1OuterWalls[Random.Range(0, Dungeon1OuterWalls.Length)];
                        GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
                        floorTile.transform.SetParent(gameBoard);
                    }
                    else if(GameController.isDungeon2)
                    {
                        selectedTile = Dungeon2OuterWalls[Random.Range(0, Dungeon2OuterWalls.Length)];
                        GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
                        floorTile.transform.SetParent(gameBoard);
                    }
                }
                else
                {
                    if(GameController.isDungeon1)
                    {
                        selectedTile = Dungeon1Floors[Random.Range(0, Dungeon1Floors.Length)];
                        GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
                        floorTile.transform.SetParent(gameBoard);
                    }
                    else if(GameController.isDungeon2)
                    {
                        selectedTile = Dungeon2Floors[Random.Range(0, Dungeon2Floors.Length)];
                        GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
                        floorTile.transform.SetParent(gameBoard);
                    }
                }
            }
        }
    }

    private void SetRandomObstaclesOnGrid(GameObject[] obstaclesArray, int minimum, int maximum)
    {
        int obstacleCount = Random.Range(minimum, maximum + 1);
        if(obstacleCount > obstaclesGrid.Count)
        {
            obstacleCount = obstaclesGrid.Count;
        }
        for(int index = 0; index < obstacleCount; index++)
        {
            GameObject selectedObstacle = obstaclesArray[Random.Range(0, obstaclesArray.Length)];
            Instantiate(selectedObstacle, SelectGridPosition(), Quaternion.identity);
        }
    }

    private Vector3 SelectGridPosition()
    {
        int randomIndex = Random.Range(0, obstaclesGrid.Count);
        Vector3 randomPosition = obstaclesGrid[randomIndex];
        obstaclesGrid.RemoveAt(randomIndex);
        return randomPosition;
    }

    public void SetUpLevel(int currentLevel)
    {
        InitializeObstaclePositions();
        if(GameController.isDungeon1)
        {
            SetRandomObstaclesOnGrid(Dungeon1WallObstacles, 3, 9);
            SetRandomObstaclesOnGrid(Dungeon1FoodItems, 1, 5);
        }
        else if(GameController.isDungeon2)
        {
            SetRandomObstaclesOnGrid(Dungeon2WallObstacles, 3, 9);
            SetRandomObstaclesOnGrid(Dungeon2FoodItems, 1, 5);
        }
        int enemyCount = (int) Mathf.Log(currentLevel, 2);
        enemyCount += 1;
        if (GameController.isDungeon1)
        {
            SetRandomObstaclesOnGrid(Dungeon1Enemies, enemyCount, enemyCount);
        }
        else if(GameController.isDungeon2)
        {
            SetRandomObstaclesOnGrid(Dungeon2Enemies, enemyCount, enemyCount);
        }
        chanceToGenerate = Random.Range(1, 11);
        if(chanceToGenerate == 5)
        {
            SetRandomObstaclesOnGrid(PowerUp, 1, 1);
        }
        Instantiate(exit, new Vector3(columns - 2, rows - 2, 0f), Quaternion.identity);
        SetUpGameBoard();
    }

}
