﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    private BoardController boardController;
    public List<Enemy> enemies;
    public int playerCurrentHealth;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    public static int currentLevel = 1;
    public AudioClip gameOverSound;
    public int removeLevelImage = 0;
    public static bool easy = false;
    public static bool medium = false;
    public static bool hard = false;
    private GameObject easyButton;
    private GameObject mediumButton;
    private GameObject hardButton;
    private GameObject player;
    private GameObject player2;
    private GameObject playerButton;
    private GameObject player2Button;
    private GameObject map;
    private GameObject Dungeon1Button;
    private GameObject Dungeon2Button;
    public static bool isPlayer1;
    public static bool isPlayer2;
    public static bool isStart;
    public static bool isDungeon1;
    public static bool isDungeon2;
    private int menu = 0;
    private string names;
    private string currentLevelphp;
    private string displayHighscores;
    private InputField enterName;
    private GameObject enterName1;
    public static bool oneDeath;
    public static bool dontMove;

    public void EnterName()
    {
        names = enterName.text;
        enterName1.SetActive(false);
        dontMove = true;
    }

    public void Dungeon1()
    {
        isDungeon1 = true;
        InitializeGame();
        Dungeon2Button.SetActive(false);
        Dungeon1Button.SetActive(false);
        playerButton.SetActive(true);
        player2Button.SetActive(true);
    }

    public void Dungeon2()
    {
        isDungeon2 = true;
        InitializeGame();
        Dungeon1Button.SetActive(false);
        Dungeon2Button.SetActive(false);
        playerButton.SetActive(true);
        player2Button.SetActive(true);
    }

    public void Player1()
    {
        menu++;
        isPlayer1 = true;
        if (menu == 1)
        {
            playerButton.SetActive(false);
            player2Button.SetActive(false);
            easyButton.SetActive(true);
            mediumButton.SetActive(true);
            hardButton.SetActive(true);
        }
        else if(menu == 2)
        {
            playerButton.SetActive(false);
            player2Button.SetActive(false);
            enterName1.SetActive(true);
        }
        playerCurrentHealth = 50;
        Player.playerHealth = playerCurrentHealth;
    }

    public void Player2()
    {
        menu++;
        Player.animator.SetTrigger("player2");
        isPlayer2 = true;
        if(menu == 1)
        {
            playerButton.SetActive(false);
            player2Button.SetActive(false);
            easyButton.SetActive(true);
            mediumButton.SetActive(true);
            hardButton.SetActive(true);
        }
        else if(menu == 2)
        {
            playerButton.SetActive(false);
            player2Button.SetActive(false);
            enterName1.SetActive(true);
        }
        playerCurrentHealth = 20;
        Player.playerHealth = playerCurrentHealth;
    }

    public void Easy()
    {
        menu++;
        easy = true;
        if(menu == 1)
        {
            easyButton.SetActive(false);
            mediumButton.SetActive(false);
            hardButton.SetActive(false);
            playerButton.SetActive(true);
            player2Button.SetActive(true);
        }
        else if(menu == 2)
        {
            easyButton.SetActive(false);
            mediumButton.SetActive(false);
            hardButton.SetActive(false);
            enterName1.SetActive(true);
        }
    }

    public void Medium()
    {
        menu++;
        medium = true;
        if (menu == 1)
        {
            easyButton.SetActive(false);
            mediumButton.SetActive(false);
            hardButton.SetActive(false);
            playerButton.SetActive(true);
            player2Button.SetActive(true);
        }
        else if (menu == 2)
        {
            easyButton.SetActive(false);
            mediumButton.SetActive(false);
            hardButton.SetActive(false);
            enterName1.SetActive(true);
        }
    }

    public void Hard()
    {
        menu++;
        hard = true;
        if (menu == 1)
        {
            easyButton.SetActive(false);
            mediumButton.SetActive(false);
            hardButton.SetActive(false);
            playerButton.SetActive(true);
            player2Button.SetActive(true);
        }
        else if (menu == 2)
        {
            easyButton.SetActive(false);
            mediumButton.SetActive(false);
            hardButton.SetActive(false);
            enterName1.SetActive(true);
        }
    }

	void Awake () {
        if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}

    public void GetScore()
    {
        string getScores = "http://bryan0573.pcriot.com/cgi-bin/getscores.php?";
        WWW scores = new WWW(getScores);
        StartCoroutine(GetScores(scores));
    }

    public void PostScores()
    {
        string postScores = "http://bryan0573.pcriot.com/cgi-bin/savescores.php?name=" + names + "&score=" + currentLevel;
        WWWForm newHighScore = new WWWForm();
        newHighScore.AddField(names, currentLevel);
        WWW postScore = new WWW(postScores, newHighScore);
        StartCoroutine(PostScores(postScore));
    }

    void Start()
    {
        oneDeath = true;
        Dungeon1Button = GameObject.Find("Dungeon 1");
        Dungeon2Button = GameObject.Find("Dungeon 2");
        map = GameObject.Find("Map");
        easyButton = GameObject.Find("Easy");
        mediumButton = GameObject.Find("Medium");
        hardButton = GameObject.Find("Hard");
        easyButton.SetActive(true);
        mediumButton.SetActive(true);
        hardButton.SetActive(true);
        playerButton = GameObject.Find("Player Button");
        player2Button = GameObject.Find("Player2 Button");
        playerButton.SetActive(false);
        player2Button.SetActive(false);
        player = GameObject.Find("Player");
        player2 = GameObject.Find("Player2");
        map.SetActive(true);
        Dungeon1Button.SetActive(true);
        Dungeon2Button.SetActive(true);
        enterName1 = GameObject.Find("InputField");
        enterName = enterName1.GetComponent<InputField>();
    }

    void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
	}

    IEnumerator PostScores(WWW www)
    {
        yield return www;
        Debug.Log(www.data);
    }
    
    IEnumerator GetScores(WWW www)
    {
        yield return www;
        levelText.text = www.text;
    }

    private void InitializeGame()
    {
        if (isDungeon1 || isDungeon2)
        {
            isStart = true;
            settingUpGame = true;
            levelImage = GameObject.Find("Level Image");
            map = GameObject.Find("Map");
            Dungeon1Button = GameObject.Find("Dungeon 1");
            Dungeon2Button = GameObject.Find("Dungeon 2");
            enterName1 = GameObject.Find("InputField");
            levelText = GameObject.Find("Level Text").GetComponent<Text>();
            if (currentLevel == 1)
            {
                
            }
            else
            {
                levelText.text = "Day: " + currentLevel;
            }
            levelImage.SetActive(true);
            map.SetActive(true);
            Dungeon1Button.SetActive(true);
            Dungeon2Button.SetActive(true);
            enemies.Clear();
            boardController.SetUpLevel(currentLevel);
            if (currentLevel == 1)
            {

            }
            else
            {
                map.SetActive(false);
                Dungeon1Button.SetActive(false);
                Dungeon2Button.SetActive(false);
                Invoke("DisableLevelImage", secondsUntilLevelStart);
            }
        }
    }

    public void DisableLevelImage()
    {
        Dungeon1Button.SetActive(false);
        Dungeon2Button.SetActive(false);
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
        if (Player.animator != null)
        {
            if (isPlayer2)
            {
                Player.animator.SetTrigger("player2");
            }
            if (isPlayer1)
            {
                Player.animator.SetTrigger("player1Normal");
            }
            else if (isPlayer2)
            {
                Player.animator.SetTrigger("player2Normal");
            }
        }
    }


    private void OnLevelWasLoaded(int levelLoaded)
    { 
        currentLevel++;
        InitializeGame();
    }

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;
        yield return new WaitForSeconds(0.2f);
        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }
        areEnemiesMoving = false;
        isPlayerTurn = true;
    }


    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        PostScores();
        GetScore();
        enterName1.SetActive(true);
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelImage.SetActive(true);
        enabled = false;
    }

}
