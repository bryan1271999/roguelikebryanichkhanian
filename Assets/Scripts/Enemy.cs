﻿using UnityEngine;
using System.Collections;

public class Enemy : MovingObject {

    private Transform player;
    public bool isEnemyStrong;
    public bool isEnemyStronger;
    private bool skipCurrentMove;
    public static Animator animator;
    public int attackDamage;
    public AudioClip enemySound1;
    public AudioClip enemySound2;

	protected override void Start () {
        GameController.Instance.AddEnemyToList(this);
        player = GameObject.FindGameObjectWithTag("Player").transform;
        skipCurrentMove = true;
        animator = GetComponent<Animator>();
        base.Start();
	}

    public void MoveEnemy()
    {
        if(skipCurrentMove)
        {
            if (GameController.isPlayer1)
            {
                if (isEnemyStrong)
                {
                    if (GameController.easy)
                    {
                        int chanceToMove = Random.Range(1, 2);
                        if (chanceToMove > 1)
                        {
                            skipCurrentMove = false;
                            return;
                        }
                    }
                    else if (GameController.medium)
                    {
                        int chanceToMove = Random.Range(1, 4);
                        if (chanceToMove > 1)
                        {
                            skipCurrentMove = false;
                            return;
                        }

                    }
                    else if (GameController.hard)
                    {
                        int chanceToMove = Random.Range(1, 6);
                        if (chanceToMove > 1)
                        {
                            skipCurrentMove = false;
                            return;
                        }
                    }
                }
                else if (isEnemyStronger)
                {
                    if (GameController.easy)
                    {
                        int chanceToMove = Random.Range(1, 4);
                        if(chanceToMove != 1)
                        {
                            skipCurrentMove = false;
                            return;
                        }
                        return;
                    }
                    else if (GameController.medium)
                    {
                        int chanceToMove = Random.Range(1, 2);
                        if (chanceToMove > 1)
                        {
                            skipCurrentMove = false;
                            return;
                        }
                    }
                    else if (GameController.hard)
                    {
                        skipCurrentMove = false;
                    }
                }
                else
                {
                    skipCurrentMove = false;
                    return;
                }
            }
            else if(GameController.isPlayer2)
            {
                skipCurrentMove = false;
            }
        }
        int xAxis = 0;
        int yAxis = 0;
        float xAxisDistance = Mathf.Abs(player.position.x - transform.position.x);
        float yAxisDistance = Mathf.Abs(player.position.y - transform.position.y);
        if(xAxisDistance > yAxisDistance)
        {
            if(player.position.x > transform.position.x)
            {
                if(GameController.isPlayer2)
                {
                    xAxis = 2;
                }
                else
                {
                    xAxis = 1;
                }
            }
            else
            {
                if(GameController.isPlayer1)
                {
                    xAxis = -1;
                }
                else
                {
                    xAxis = -2;
                }
            }
        }
        else
        {
            if(player.position.y > transform.position.y)
            {
                if(GameController.isPlayer1)
                {
                    yAxis = 1;
                }
                else
                {
                    yAxis = 2;
                }
           }
            else
            {
                if(GameController.isPlayer1)
                {
                    yAxis = -1;
                }
                else
                {
                    yAxis = -2;
                }
            }
        }
        Move<Player>(xAxis, yAxis);
        skipCurrentMove = true;
    }

    protected override void HandleCollision<T>(T Component)
    {
        Player player = Component as Player;
        player.TakeDamage(attackDamage);
        animator.SetTrigger("enemyAttack");
        SoundController.Instance.PlaySingle(enemySound1, enemySound2);
    }

}
