﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

    public static Animator animator;
    public AudioClip movementSound1;
    public AudioClip movementSound2;
    public AudioClip chopSound1;
    public AudioClip chopSound2;
    public AudioClip fruitSound1;
    public AudioClip fruitSound2;
    public AudioClip sodaSound1;
    public AudioClip sodaSound2;
    public static int attackPower = 1;
    public Text healthText;
    private int healthPerFruit = 5;
    private int healthPerSoda = 10;
    private int healthPerFlower = 20;
    private int secondsUntilNextLevel = 1;
    public static int playerHealth;
    private int minScreenWidth;
    private int maxScreenWidth;
    private int tileSize;
    public int xAxis = 0;
    public int yAxis = 0;
    private static int counterHealth;
    private static string keyCode;
    private static int keyCodes;
    public static int killAllEnemies;

    protected override void Start()
    {
        base.Start();
        playerHealth = GameController.Instance.playerCurrentHealth;
        animator = GetComponent<Animator>();
        healthText.text = "Health: " + playerHealth;
    }

    private void OnDisable()
    {
        GameController.Instance.playerCurrentHealth = playerHealth;
    }
	
    private void BalanceHealth()
    {
        if(GameController.isStart)
        {
                playerHealth += 1;
                Invoke("DecreaseHealth", 3);
        }
    }

    private void DecreaseHealth()
    {
        playerHealth -= 1;
    }
 
	void Update () {
        if(GameController.Instance.isPlayerTurn == false)
        {
            if(GameController.isPlayer1)
            {
                return;
            }
            else if(GameController.isPlayer2)
            {
                Invoke("BalanceHealth", 1);

            }
        }
        CheckIfGameOver();
        if (GameController.dontMove)
        {
            if (Input.touchCount > 0)
            {
                Vector2 touchPosition = Input.GetTouch(0).position;
                xAxis = (int)touchPosition.x;
                yAxis = (int)touchPosition.y;
                GetScreenSize();
                if (xAxis >= minScreenWidth && xAxis <= maxScreenWidth)
                {
                    ExecuteTouch();
                }
                else
                {
                    xAxis = 0;
                    yAxis = 0;
                }
            }
            else
            {
                xAxis = (int)Input.GetAxisRaw("Horizontal");
                yAxis = (int)Input.GetAxisRaw("Vertical");
                if (xAxis != 0)
                {
                    yAxis = 0;
                }
            }
        }
            if (xAxis != 0 || yAxis != 0)
            {
                SoundController.Instance.PlaySingle(movementSound1, movementSound2);
                Move<Wall>(xAxis, yAxis);
                GameController.Instance.isPlayerTurn = false;
                playerHealth--;
                healthText.text = "Health: " + playerHealth;
                GameController.Instance.isPlayerTurn = false;
                healthText.text = "Health: " + playerHealth;
            }
    }

    private void GetScreenSize()
    {
        int screenWidth = Screen.currentResolution.width;
        int screenHeight = Screen.currentResolution.height;
        minScreenWidth = (screenWidth - screenHeight) / 2;
        maxScreenWidth = minScreenWidth + screenHeight;
        tileSize = screenHeight / 10;
    }

    private void ExecuteTouch()
    {
        Vector2 currentPosition = this.getPosition();
        CorrectForScreens();
        if(Mathf.Abs(xAxis - (int) currentPosition.x) > Mathf.Abs (yAxis - (int) currentPosition.y))
        {
            MoveHorizontal(currentPosition);
        }
        else
        {
            MoveVertical(currentPosition);
        }
    }

    private void CorrectForScreens()
    {
        xAxis -= minScreenWidth;
        xAxis /= tileSize;
        yAxis /= tileSize;
    }

    private void MoveHorizontal(Vector2 currentPosition)
    {
            yAxis = 0;
            if (xAxis - (int)currentPosition.x > 0)
            {
                xAxis = 1;
            }
            else if (xAxis - (int)currentPosition.x < 0)
            {
                xAxis = -1;
            }
            else
            {
                xAxis = 0;
            }
    }

    private void MoveVertical(Vector2 currentPosition)
    {
            xAxis = 0;
            if (yAxis - currentPosition.y > 0)
            {
                yAxis = 1;
            }
            else if (yAxis - currentPosition.y < 0)
            {
                yAxis = -1;
            }
            else
            {
                yAxis = 0;
            }
    }

    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if (objectPlayerCollidedWith.tag == "Exit")
        {
            Invoke("LoadNewLevel", secondsUntilNextLevel);
            enabled = false;
        }
        else if (objectPlayerCollidedWith.tag == "Fruit")
        {
            playerHealth += healthPerFruit;
            healthText.text = "+" + healthPerFruit + "\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Flower")
        {
            playerHealth -= healthPerFlower;
            healthText.text = "-" + healthPerFlower + "\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Soda")
        {
            playerHealth += healthPerSoda;
            healthText.text = "+" + healthPerSoda + "\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Shrub of Invisibility")
        {
            animator.SetTrigger("invisibility");
            GameController.Instance.enemies.Clear();
        }
    }


    public void LoadNewLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    protected override void HandleCollision<T>(T Component)
    {
        Wall wall = Component as Wall;
        animator.SetTrigger("playerAttack");
        SoundController.Instance.PlaySingle(chopSound1, chopSound2);
        wall.DamageWall(attackPower);
    }

    private void PlayerAttack()
    {
        if (Input.GetKey(KeyCode.Alpha1) && keyCodes == 1 || Input.GetKey(KeyCode.Alpha2) && keyCodes == 2 || Input.GetKey(KeyCode.Alpha3) && keyCodes == 3 || Input.GetKey(KeyCode.Alpha4) && keyCodes == 4 || Input.GetKey(KeyCode.Alpha5) && keyCodes == 5)
        {
            killAllEnemies++;
            playerHealth += counterHealth;
            healthText.text = "Health: " + playerHealth;
            animator.SetTrigger("playerAttack");
            if (killAllEnemies == 3)
            {
                killAllEnemies = 0;
                GameController.Instance.enemies.Clear();
            }
        }
        else
        {
            healthText.text = "Health: " + playerHealth;
        }
    }

    public void TakeDamage(int damageReceived)
    {
        counterHealth = damageReceived;
        playerHealth -= damageReceived;
        healthText.text = "-" + damageReceived + " Health \n" + " Health: " + playerHealth;
        animator.SetTrigger("playerHurt");
        if(GameController.isPlayer1)
        {
            keyCodes = Random.Range(1, 6);
            keyCode = "" + keyCodes;
            healthText.text = "Hold " + keyCode + " to counter";
            Invoke("PlayerAttack", 0.7f);
        }
    }

    private void CheckIfGameOver()
    {
        if(playerHealth <= 0)
        {
            if(GameController.oneDeath == true)
            {
                GameController.oneDeath = false;
                GameController.Instance.GameOver();
            }
        }
    }

}
